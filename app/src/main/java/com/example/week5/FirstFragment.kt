package com.example.week5

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.core.view.children
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.week5.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private lateinit var binding : FragmentFirstBinding
    private lateinit var adapter1 : RecyclerViewAdapterFirst
    private val viewModel : MainViewModel by viewModels()
    private var listOfLists = mutableListOf<MutableList<MyModel>>()
    private var helperList = mutableListOf<String?>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentFirstBinding.inflate(inflater, container, false)

        init()

        binding.registerButton.setOnClickListener {
            check()
        }

        return binding.root
    }

    private fun init(){
        viewModel.init()
        observes()
        initRecycler()
    }

    private fun initRecycler(){
        adapter1 = RecyclerViewAdapterFirst(listOfLists)
        binding.recycler1.layoutManager = LinearLayoutManager(this.context)
        binding.recycler1.adapter = adapter1
    }

    private fun observes(){
        var i = 0
        viewModel.getMyLiveData().observe(viewLifecycleOwner, { it ->
            listOfLists = it.toMutableList()
        })
        binding.recycler1.children.forEach { it ->
            it.findViewById<RecyclerView>(R.id.recycler2).children.forEach {
                val editTextText = it.findViewById<EditText>(R.id.inputText)
                helperList.add(editTextText.text.toString())
            }
        }
        listOfLists.forEach { it ->
            it.forEach {
                viewModel.myContainer1[it.fieldId] = helperList[i]
                i++
                viewModel.myContainer2[it.fieldId] = it.required
                viewModel.myContainer3[it.fieldId] = it.hint
            }
        }
    }

    private fun check(){
        viewModel.myContainer2.forEach{
            if (viewModel.myContainer2[it.component1()] == true){
                if(viewModel.myContainer1[it.component1()].isNullOrBlank()){
                    Toast.makeText(requireActivity(), "This field - ${viewModel.myContainer3[it.component1()]} MUST be filled", Toast.LENGTH_LONG).show()
                }
            }
        }
    }









}