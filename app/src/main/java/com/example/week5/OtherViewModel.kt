package com.example.week5

import androidx.lifecycle.ViewModel

class OtherViewModel : ViewModel(){

    companion object {
        var container = mutableMapOf<String, String>()

        fun adder(key : String, value : String){
            container[key] = value
        }
    }

}