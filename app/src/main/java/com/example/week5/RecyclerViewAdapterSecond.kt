package com.example.week5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.week5.databinding.Recyclerview1Binding

class RecyclerViewAdapterSecond (private val list: MutableList<MyModel>) : RecyclerView.Adapter<RecyclerViewAdapterSecond.ItemViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(Recyclerview1Binding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ItemViewHolder(private val binding : Recyclerview1Binding) : RecyclerView.ViewHolder(binding.root), View.OnFocusChangeListener{
        private lateinit var model: MyModel
        fun bind(){
            binding.inputText.hint = model.hint
            binding.inputText.onFocusChangeListener = this
            if (OtherViewModel.container.containsKey(model.hint)) {
                binding.inputText.setText(OtherViewModel.container[model.hint])
            }
        }

        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            if(!hasFocus && binding.inputText.text.toString().isNotBlank()){
                OtherViewModel.adder(model.hint, binding.inputText.text.toString())
            }
        }

    }
}