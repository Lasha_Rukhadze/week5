package com.example.week5

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    private const val BASE_URL = "https://run.mocky.io"

    fun retrofitService() : RetrofitRepository {
        return Retrofit.Builder().baseUrl(
            BASE_URL
        ).addConverterFactory(GsonConverterFactory.create()).build().create(RetrofitRepository::class.java)
    }
}