package com.example.week5

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.week5.databinding.RecyclerBaseBinding

class RecyclerViewAdapterFirst(private val myList: MutableList<MutableList<MyModel>>) : RecyclerView.Adapter<RecyclerViewAdapterFirst.ItemViewHolder>() {
    //private val myList = mutableListOf(mutableListOf<MyModel>())

    private val viewPool = RecyclerView.RecycledViewPool()
    lateinit var adapter2 : RecyclerViewAdapterSecond

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            RecyclerBaseBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int {
        return myList.size
    }

    inner class ItemViewHolder(private val binding: RecyclerBaseBinding) :
        RecyclerView.ViewHolder(binding.root) {

            fun bind(){
                binding.recycler2.apply {
                    adapter2 = RecyclerViewAdapterSecond(myList[adapterPosition])
                    binding.recycler2.layoutManager = LinearLayoutManager(binding.recycler2.context, LinearLayoutManager.VERTICAL, false)
                    binding.recycler2.adapter = adapter2
                    setRecycledViewPool(viewPool)
                }
            }


    }

}