package com.example.week5

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    val myContainer1 = mutableMapOf<Int, String?>()
    val myContainer2 = mutableMapOf<Int, Boolean>()
    val myContainer3 = mutableMapOf<Int, String>()


    private val myLiveData = MutableLiveData<MutableList<MutableList<MyModel>>>().apply {
        mutableListOf(mutableListOf<MyModel>())
    }

    //val _newsLiveData : LiveData<List<NewsModel>> = newsLiveData
    fun getMyLiveData() : LiveData<MutableList<MutableList<MyModel>>> {
        return  myLiveData
    }

    private val loadingLiveData = MutableLiveData<Boolean>().apply {
        true
    }

    //val _loadingLiveData : LiveData<Boolean> = loadingLiveData

    /*fun getLoadingLiveData() : LiveData<Boolean>{
        return loadingLiveData
    }*/

    fun init(){
        viewModelScope.launch(Dispatchers.Default) {
            getInfos()
        }
    }

    private suspend fun getInfos(){
        val result = RetrofitService.retrofitService().getInfo()
        if (result.isSuccessful){
            val infos = result.body()
            myLiveData.postValue(infos)
        }
        else {
            result.code()
        }
       // loadingLiveData.postValue(false)
    }
}